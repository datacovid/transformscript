package dto
type Date struct {

	Day int64
	Month int64
	Year int64
	FullDate string
	Value int64
}