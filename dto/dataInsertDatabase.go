package dto


type InsertDocumentDate struct {


	Day int64 `bson:day`
	Month int64 `bson:month`
	Year int64 `bson:year`
	FullDate string `bson:fulldate`
	Value int64 `bson:value`

}