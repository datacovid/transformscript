package dto

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)
type Document struct {


	ID primitive.ObjectID `bson:"_id,omitempty"`
	DescDose string `bson:"descricao_dose"`
	DataAplicacao string `bson:"data_aplicacao"`

}