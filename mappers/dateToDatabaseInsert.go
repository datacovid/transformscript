package mappers

import (
	"transformCovid/dto"
)

func DateToDocument (date *dto.Date) dto.InsertDocumentDate  {

	var documentInsert dto.InsertDocumentDate

	documentInsert.Day = date.Day
	documentInsert.Month = date.Month
	documentInsert.Year = date.Year
	documentInsert.FullDate = date.FullDate 
	documentInsert.Value = date.Value
	return documentInsert


}