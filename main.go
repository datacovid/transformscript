package main
import (
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/options"
	"context"
	"time"
	"log"
	"transformCovid/dto"
	"transformCovid/mappers"
	"strconv"
)

func getConnection() (*mongo.Collection, *mongo.Client){

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://root:rootpassword@localhost:27017/mydb1?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false"))
	if(err != nil){
		panic(err)
	}
	//defer client.Disconnect(ctx)
	database := client.Database("testGolang")
	vacinaCol := database.Collection("vacinaCol")
	return vacinaCol, client
}

func getConnectionRemote() (*mongo.Collection, *mongo.Client){
	ctx, cancel := context.WithTimeout(context.Background(), 10000000*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("remoteAddr",))
	if(err != nil){
		panic(err)
	}
	//defer client.Disconnect(ctx)
	database := client.Database("testGolang")
	dateCol := database.Collection("dateCol")
	return dateCol, client
}


func createObjectData ( vac string ) *dto.Date{

	var date dto.Date
	date.FullDate = vac[:len(vac)-14]
	date.Year, _ = strconv.ParseInt(vac[:len(vac)-20],10,64)
	date.Month, _ = strconv.ParseInt(vac[5:len(vac)-17],10,64)
	date.Day, _ = strconv.ParseInt(vac[8:len(vac)-14],10,64)
	date.Value = 1
	return &date
}

func insertDate ( mapObj map[string]*dto.Date) {
	dateCol, _ := getConnectionRemote()
	ctx, cancel := context.WithTimeout(context.Background(), 100000*time.Second)
	for _, m := range mapObj {
		fmt.Println("Inseri 1")
		document := mappers.DateToDocument(m)
		_, err := dateCol.InsertOne(ctx, document)
		if(err != nil){
			panic(err)
		}

		
	}
	defer cancel()

}

func getData (skipedUnit int64, i int, j int, mapObj map[string]*dto.Date, connVacinaCol *mongo.Collection, client *mongo.Client) {
	
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	opts := options.Find().SetSkip(skipedUnit).SetLimit(10000)
	cursor, err := connVacinaCol.Find(ctx, bson.M{}, opts)
	if err != nil {
		log.Fatal(err)
	}
	var vac []dto.Document
	if err = cursor.All(ctx, &vac); err != nil {
		log.Fatal(err)
	}
	for _, v := range vac{
		dateObj:=createObjectData(v.DataAplicacao)
		_ , ok := mapObj[dateObj.FullDate]
		if(!ok){
			mapObj[dateObj.FullDate] = dateObj
		}else{
			mapObj[dateObj.FullDate].Value = mapObj[dateObj.FullDate].Value + 1
		}
		
		if(v.DescDose ==  "    2ª Dose"){
			i++
		}else {j++}
	}
	fmt.Println(i+j)
	if(i+j >= 35678796){
		fmt.Println("Entrei aqui")
		defer client.Disconnect(ctx)
		insertDate( mapObj)
		return
	}
	getData(skipedUnit + 10000, i, j, mapObj, connVacinaCol, client)
	
}

func main() {
	var i = 0
	var j = 0
	connVacinaCol, client := getConnection()
	mapObj:=make(map[string]*dto.Date)
	fmt.Println("Começando")
	getData(0, i, j, mapObj, connVacinaCol, client)

	
	
}